#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
NetCDF utilities.
"""
__author__ = "Brian Højen-Sørensen"
__contact__ = "brs@fcoo.dk"
__copyright__ = "Copyright 2012, Danish Defence"
__license__ = "MIT License"
__version__ = "1.2.4"

# Standard library imports
import re
from datetime import datetime
from dateutil.parser import parse

# External imports
import numpy as np
import netCDF4

def islsmask(lsmask):
    """Returns True if input is a land-sea mask variable."""
    unam = ['land-sea mask']
    if hasattr(lsmask, 'standard_name') and getattr(lsmask, 'standard_name').lower() in unam:
        return True
    elif hasattr(lsmask, 'long_name') and getattr(lsmask, 'long_name').lower() in unam:
        return True
    return False

def isrlon(lon):
    """Returns True if input is a rotated longitude variable."""
    ulon = ['degrees']
    unam = ['grid_longitude', 'longitude']
    if hasattr(lon, 'units') and getattr(lon, 'units').lower() in ulon \
    and hasattr(lon, 'standard_name') and getattr(lon, 'standard_name').lower() in unam:
        return True
    return False

def islon(lon):
    """Returns True if input is a longitude variable."""
    ulon = ['degrees_east', 'degree_east', 'degree_e', 'degrees_e', \
            'degreee', 'degreese']
    if hasattr(lon, 'units') and getattr(lon, 'units').lower() in ulon:
        return True
    return False

def isrlat(lat):
    """Returns True if input is a rotated longitude variable."""
    ulat = ['degrees']
    unam = ['grid_latitude', 'latitude']
    if hasattr(lat, 'units') and getattr(lat, 'units').lower() in ulat \
    and hasattr(lat, 'standard_name') and getattr(lat, 'standard_name').lower() in unam:
        return True
    return False

def islat(lat):
    """Returns True if input is a latitude variable."""
    ulat = ['degrees_north', 'degree_north', 'degree_n', 'degrees_n', \
            'degreen', 'degreesn']
    if hasattr(lat, 'units') and getattr(lat, 'units').lower() in ulat:
        return True
    return False

def isvertical(ver):
    """Returns True if input is a vertical variable."""
    upressure = ['bar', 'atmosphere', 'atm', 'pascal', 'pa', 'hpa']
    #uother = ['meter', 'metre', 'm', 'kilometer', 'km', 'level', 'layer', \
    #           'sigma_level']
    u = getattr(ver, 'units', '')
    if hasattr(ver, 'positive') or u.lower() in upressure:
        return True
    # GETM hack
    if u.lower() == 'level':
        return True
    return False

def istime(time):
    """\
    Returns True if input is a time dimension according to the C/F convention.
    """
    u = getattr(time, 'units', '')
    if re.compile(" since ").search(u.lower()):
        return True
    return False

def nctime(nc_time_var, epoch=False):
    """\
    Converts an array of NetCDF times to datetime objects. 
    The method fixes some deviations from the C/F convention.
    """
    tunits = getattr(nc_time_var, "units").strip()
    tunits = tunits.replace('/','-') # Fix non C/F time units

    # Convert datetime strings in valid_min and valid_max to numbers
    if hasattr(nc_time_var, 'valid_min') and isinstance(nc_time_var.valid_min, str):
        nc_time_var.valid_min = netCDF4.date2num(parse(nc_time_var.valid_min),tunits)
    if hasattr(nc_time_var, 'valid_max') and isinstance(nc_time_var.valid_max, str):
        nc_time_var.valid_max = netCDF4.date2num(parse(nc_time_var.valid_max),tunits)

    try:
        calendar = getattr(nc_time_var, "calendar")
    except:
        calendar = "standard"
    if epoch:
        t = netCDF4.num2date(0, tunits, calendar=calendar)
    else:
        t = netCDF4.num2date(nc_time_var[:], tunits, calendar=calendar)
    if None in t:
        raise ValueError('Error: one or more time values are outside of valid_min or valid_max')
    t = [datetime.strptime(str(dt), '%Y-%m-%d %H:%M:%S') for dt in t]
    t = [datetime.fromisoformat(dt.isoformat()) for dt in t]
    return np.array([dt.replace(microsecond=0) for dt in t])


def dimensions(nc, s=None):
    """\
    Extracts dimension arrays from a netcdf variable object.
    Vertical dimensions are multiplied with -1 if they have the
    positive="down" attribute.
    Returns None for non-present dimensions.
    """
    if s is not None:
        ncvar = nc.variables[s]
        dims = ncvar.dimensions
    else:
        dims = nc.dimensions.keys()
    x = None
    y = None
    z = None
    t = None
    for d in dims:
        if d in nc.variables.keys():
            ncvar = nc.variables[d]
            if istime(ncvar):
                t = nctime(ncvar)
            elif isvertical(ncvar):
                # GETM hack
                u = getattr(ncvar, 'units', '')
                if u.lower() == 'level':
                    pass
                positive = getattr(ncvar, "positive", "up")
                if positive == 'down':
                    z = -1.0*ncvar[:]
                else:
                    z = ncvar[:]
            elif islat(ncvar):
                y = ncvar[:]
            elif isrlat(ncvar):
                y = ncvar[:]
            elif islon(ncvar):
                x = ncvar[:]
            elif isrlon(ncvar):
                x = ncvar[:]
    return x, y, z, t

def ncdimensions(nc, s=None):
    """\
    Extracts dimension variables from a netcdf file object.
    Vertical dimensions are multiplied with -1 if they have the
    positive="down" attribute.
    Returns None for non-present dimensions.
    """
    if s is not None:
        ncvar = nc.variables[s]
        dims = ncvar.dimensions
    else:
        dims = nc.dimensions.keys()
    x = None
    y = None
    z = None
    t = None
    for d in dims:
        if d in nc.variables.keys():
            ncvar = nc.variables[d]
            if istime(ncvar):
                t = nctime(ncvar)
            elif isvertical(ncvar):
                # GETM hack
                u = getattr(ncvar, 'units', '')
                if u.lower() == 'level':
                    # Make 4D vertical levels
                    h = nc.variables['h']
                    h = np.ma.masked_values(h[:], h._FillValue)
                    assert len(h.shape) == 4, 'h is not 4-dimensional'
                    nt, nz, ny, nx = h.shape
                    h_int = np.zeros((nt,ny,nx))

                    for i in range(nz-1,-1,-1):
                        h_current = np.ma.array(h[:,i,:,:], copy=True)
                        h[:,i,:,:] = h_int + 0.5*h_current
                        h_int = h_int + h_current
                    z = -1.0*h
                else:
                    z = ncvar[:]
                positive = getattr(ncvar, "positive", "up")
                if positive == 'down':
                    z = -1.0*z[:]
            elif islat(ncvar):
                y = ncvar[:]
            elif islon(ncvar):
                x = ncvar[:]
    return x, y, z, t

def rotation(nc):
    """\
    Extracts grid rotation from a netcdf file object.
    Returns None if not rotated.
    """
    r = None
    if 'rotated_pole' in nc.variables.keys():
        pole = nc.variables['rotated_pole']
        if hasattr(pole, 'north_pole_grid_longitude'):
            r = [pole.grid_north_pole_latitude, pole.grid_north_pole_longitude, pole.north_pole_grid_longitude]
        else:
            r = [pole.grid_north_pole_latitude, pole.grid_north_pole_longitude, 0.0]
    return r

def longitude(nc, s=None, rotated=False, data=True):
    """\
    Extract longitude variable from a netcdf file object.
    If rotated=True check for rotated longitude.
    Returns None if longitude not present.
    """
    coords = None
    if s is not None:
        try:
            coords = nc.variables[s].coordinates.split()
        except:
            pass
    x = None
    for v in nc.variables.keys():
        ncvar = nc.variables[v]
        if coords:
            if v in coords:
                if isrlon(ncvar) or islon(ncvar):
                    x = ncvar
        else:
            if rotated:
                if isrlon(ncvar):
                    x = ncvar
            else:
                if islon(ncvar):
                    x = ncvar
        if x is not None:
            break
    if data:
        return x[:]
    else:
        return x

def latitude(nc, s=None, rotated=False, data=True):
    """\
    Extracts latitude variable from a netcdf file object.
    If rotated=True check for rotated latitude.
    Returns None if latitude not present.
    """
    coords = None
    if s is not None:
        try:
            coords = nc.variables[s].coordinates.split()
        except:
            pass
    y = None
    for v in nc.variables.keys():
        ncvar = nc.variables[v]
        if coords:
            if v in coords:
                if isrlat(ncvar) or islat(ncvar):
                    y = ncvar
        else:
            if rotated:
                if isrlat(ncvar):
                    y = ncvar
            else:
                if islat(ncvar):
                    y = ncvar
        if y is not None:
            break
    if data:
        return y[:]
    else:
        return y

def lsmask(nc):
    """\
    Extracts land-sea mask variable from a netcdf file object.
    Returns None if lsmask not present.
    """
    m = None
    for v in nc.variables.keys():
        ncvar = nc.variables[v]
        if islsmask(ncvar):
            m = ncvar[:]
    return m 

def vertical(nc, s, tslice=slice(0,None), yslice=slice(0,None), xslice=slice(0,None)):
    """\
    Extracts vertical interface depths from a netcdf file object.
    Multiplied with -1 if the variable has the positive="down" attribute.
    Returns None if vertical dimension is not present.
    """
    # NOTE: This method is responsible for 48% (2012-06-18) of the 
    # total request time for the data/profile method.
    # Potential solutions: caching (need to move the slicing stuff), 
    # optimization

    ncvar = nc.variables[s]
    dims = ncvar.dimensions
    z = None
    for d in dims:
        if d in nc.variables.keys():
            ncvar = nc.variables[d]
            if isvertical(ncvar):
                # GETM hack
                u = getattr(ncvar, 'units', '')
                if u.lower() == 'level':
                    # Make 4D vertical levels
                    h = nc.variables['h']
                    h = np.ma.masked_values(h[tslice,:,yslice,xslice], 
                                            h._FillValue)
                    assert len(h.shape) == 4, 'h is not 4-dimensional'
                    nt, nz, ny, nx = h.shape
                    #h_int = np.zeros((nt,ny,nx))
                    #h_int_next = np.zeros((nt,ny,nx))
                    # Calculate interface depths
                    z = np.ma.cumsum(h[:,::-1,:,:], axis=1)
                    ## Center the depths
                    #z = z[:,::-1,:,:] - 0.5*h[:,:,:,:]
                    h[:,1:nz,:,:] = z[:,0:nz-1,:,:]
                    h[:,0,:,:] = 0.0
                    z = -h[:,::-1,:,:]
                else:
                    z = ncvar[:]
                    positive = getattr(ncvar, "positive", "up")
                    if positive == 'down':
                        z = -z
    return z

def temporal(nc, s=None, data=True, epoch=False):
    """
    Extracts temporal variable from a netcdf file object.
    Returns None if time is not present.
    """
    if s is not None:
        dims = nc.variables[s].dimensions
    else:
        dims = nc.dimensions.keys()
    t = None
    for v in nc.variables.keys():
        ncvar = nc.variables[v]
        dim = False
        for d in ncvar.dimensions:
            if d in dims:
                dim = True
        if dim: # dimensions match
            if istime(ncvar):
                t = ncvar
    if t is not None and data:
        return nctime(t, epoch=epoch)
    else:
        return t

def time(nc, s=None, data=True, epoch=False):
    """Wrapper for ncutils.temporal"""
    return temporal(nc, s, data, epoch)

def dimension_units(nc, ncvar):
    """Extracts dimension units from a netcdf variable object."""
    dims = ncvar.dimensions
    x = None
    y = None
    z = None
    t = None
    for d in dims:
        if d in nc.variables.keys():
            ncvar = nc.variables[d]
            units = getattr(ncvar, 'units')
            if istime(ncvar):
                t = units
            elif isvertical(ncvar):
                z = units
            elif islat(ncvar):
                y = units
            elif isrlat(ncvar):
                y = units
            elif islon(ncvar):
                x = units
            elif isrlon(ncvar):
                x = units
    return x, y, z, t

def dimension_short_names(nc):
    """Extracts dimension short names from a netcdf variable object."""
    dims = nc.dimensions
    x = None
    y = None
    z = None
    t = None
    for d in dims:
        if d in nc.variables.keys():
            ncvar = nc.variables[d]
            if istime(ncvar):
                t = d
            elif isvertical(ncvar):
                z = d
            elif islat(ncvar):
                y = d
            elif isrlat(ncvar):
                y = d
            elif islon(ncvar):
                x = d
            elif isrlon(ncvar):
                x = d
    return x, y, z, t

def copy_var(nc_in, nc_out, s, copy_data=True):
    """Copies a netcdf variable object from nc_in to nc_out."""
    if s in nc_in.variables:
        ncvar = nc_in.variables[s]
        if s in nc_out.variables:
            v = nc_out.variables[s]
        else:
            v = nc_out.createVariable(s, ncvar.dtype, ncvar.dimensions)
            v.setncatts(ncvar.__dict__)
        if copy_data:
            v[:] = ncvar[:]
    else:
        raise StandardError('Variable not found' % (s))

def copy_dim(nc_in, nc_out, s):
    """Copies a netcdf dimension object from nc_in to nc_out."""
    if s in nc_in.dimensions:
        dimension = nc_in.dimensions[s]
        nc_out.createDimension(s, (len(dimension) if not dimension.isunlimited() else None))
    else:
        raise StandardError('Dimension not found' % (s))

def variable_by_standard_name(nc, snl, data=True):
    """\
    Extract variable from a NetCDF file object using standard name.
    """
    snl = list(np.array(snl).flat)
    for sn in snl:
        var_list = nc.get_variables_by_attributes(standard_name=sn)
        if len(var_list) > 1:
            raise ValueError('Multiple variables with standard_name: {}: {}'.format(sn, var_list))
        elif len(var_list) == 1:
            if data:
                return var_list[0][:]
            else:
                return var_list[0]
    return None

def resettime(nc, epoch=None, units=None, calendar='proleptic_gregorian'):
    """Reset time variable and fix any round of errors"""
    nctime = temporal(nc, data=False)
    time = temporal(nc)
    if epoch is None:
        if 'epoch' in nc.ncattrs():
            epoch = datetime.strptime(nc.epoch, "%Y-%m-%dT%H:%M:%SZ")
        else:
            epoch = time[0]
    if units is None:
        units = nctime.units.split(' ')[0]
    nctime.units = '{} since {}'.format(units, epoch)
    nctime.long_name = 'time'
    nctime.standard_name = 'time'
    nctime.calendar = calendar
    for attr in nctime.ncattrs():
        if attr in ['conventions']:
            nctime.delncattr(attr)
    nctime[:] = netCDF4.date2num(time, nctime.units, calendar=nctime.calendar)
    nc.epoch = epoch.strftime("%Y-%m-%dT%H:%M:%SZ").encode('ascii')
