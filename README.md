# Ncutils
Module providing a set of utilities for accessing NetCDF data.

## Installation
~~~~bash
git clone https://gitlab.com/FCOO/ncutils.git <TARGETDIR>/ncutils
~~~~

## Setup
Add path to PYTHONPATH by adding line to .bashrc or similar:
~~~~bash
export PYTHONPATH="${PYTHONPATH}:<TARGETDIR>/ncutils"
~~~~