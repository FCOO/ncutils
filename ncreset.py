#!/usr/bin/env python3
# -*- coding: utf-8 -*-
__author__ = "Brian Højen-Sørensen"
__contact__ = "brs@fcoo.dk"
__copyright__ = "Copyright 2020, Danish Defence"
__license__ = "MIT License"
__version__ = "1.0"
"""
Purpose: Reset time variable to epoch and fix rounding errors.

Usage: Called from command line. run python3 ncreset.py -h for further
    information.
"""

# Standard library imports
import sys
import shutil
import os
import argparse
import datetime

# External library imports
import netCDF4

# Local library imports
from ncutils import ncutils

def ncreset():
    # Get my own name
    scriptname = os.path.basename(__file__)

    # Get command line arguments
    parser = argparse.ArgumentParser(
        description ="""Reset time variable to epoch and fix rounding errors""")

    parser.add_argument(dest="infile", type=str, action="store",
                        help="path to source file.")

    parser.add_argument(dest="outfile", type=str, action="store",
                        help="filename of the output file.")

    parser.add_argument('-e','--epoch', dest="epoch", type=str, action="store",
                        help="Date time for desired epoch. Default is global \
                              epoch variable (first) or time origin (second).")

    parser.add_argument('-u', '--units', dest="units", type=str, action="store",
                        help="Desired time units. Default is same as input.")

    parser.add_argument('-c', '--calendar', dest="calendar", type=str, action="store",
                        default='proleptic_gregorian',
                        help="Desired calendar. Default is Proleptic Gregorian.")

    parser.add_argument("-O", "--overwrite", dest="overwrite",
                        action="store_true", default=False,
                        help="Overwrite output file if it exist (or append to \
                              input file).")

    args      = parser.parse_args()
    infile    = args.infile
    outfile   = args.outfile
    epoch     = args.epoch
    units     = args.units
    calendar  = args.calendar
    overwrite = args.overwrite

    # Does the source file actually exist
    if not os.path.isfile(infile):
        sys.exit(scriptname+': input file: '+infile+': no such file.')

    # Check if we are trying to overwrite any files (unless overwrite = True)
    if not overwrite:
        if os.path.isfile(outfile):
            sys.exit(scriptname+': '+outfile+': file exists. Choose different name or use -O / --overwrite')

    # Is units valid
    if units not in [None, 'seconds','minutes','hours','days','years']:
        sys.exit('{}: Selected units not recognized: {}. Must be seconds, minutes, hours, days, or years.'.format(scriptname, units))

    # Is calendar valid
    if calendar not in [None, 'gregorian', 'standard', 'proleptic_gregorian',
                        'noleap', '365_day', 'all_leap', '366_day', 
                        '360_day', 'julian']:
        sys.exit('{}: Selected units not recognized: {}. Must be seconds, minutes, hours, days, or years.'.format(scriptname, units))

    # Is epoch a valid datetime
    if epoch is not None:
        try:
            epoch = datetime.datetime.strptime(epoch, '%Y-%m-%d %H:%M:%S')
        except ValueError:
            sys.exit('{}: Epoch not understood: {}. Must be YYYY-MM-DD hh:mm:ss'.format(scriptname, epoch))

    if infile != outfile: # Copy to <outfile>
        shutil.copyfile(infile, outfile)

    # Open destination file
    with netCDF4.Dataset(outfile, mode='a') as ncin:
        ncutils.resettime(ncin, epoch, units, calendar)

if __name__ == "__main__":
    ncreset()
