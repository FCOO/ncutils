#!/usr/bin/env python3
# -*- coding: utf-8 -*-
__author__ = "Brian Højen-Sørensen"
__contact__ = "brs@fcoo.dk"
__copyright__ = "Copyright 2020, Danish Defence"
__license__ = "MIT License"
__version__ = "1.0.0"
"""
Purpose: Add epoch attribute in desired format to NetCDF file.

Usage: Called from command line. run python3 ncepoch.py -h for further
    information.
"""

# Standard library imports
import sys
import shutil
import os
import argparse
from datetime import datetime

# External library imports
import netCDF4

def ncepoch():
    # Get my own name
    scriptname = os.path.basename(__file__)

    # Get command line arguments
    parser = argparse.ArgumentParser(
        description ="""Add epoch attribute in desired format to NetCDF file""")

    parser.add_argument(dest="infile", type=str, action="store", default="",
                        help="path to source file.")

    parser.add_argument(dest="outfile", type=str, action="store", default="",
                        help="filename of the output file.")

    parser.add_argument("-e","--epoch", dest="epoch", type=str,
                        help="Epoch to be added. Set format with --input-format.")

    parser.add_argument("--input-format", dest="input_format", type=str,
                        help="Epoch format (input).")

    parser.add_argument("--epoch-format", dest="epoch_format", type=str,
                        help="Epoch format (output).")

    parser.add_argument("-O", "--overwrite", dest="overwrite",
                        action="store_true", default=False,
                        help="Overwrite output file if it exist (or append to \
                              input file).")

    args      = parser.parse_args()
    infile    = args.infile
    outfile   = args.outfile
    epoch     = args.epoch
    informat  = args.input_format
    outformat = args.epoch_format
    overwrite = args.overwrite

    # Does the source file actually exist
    if not os.path.isfile(infile):
        sys.exit(scriptname+': input file: '+infile+': no such file.')

    # Check if we are trying to overwrite any files (unless overwrite = True)
    if not overwrite:
        if os.path.isfile(outfile):
            sys.exit(scriptname+': '+outfile+': file exists. Choose different name or use -O / --overwrite')

    if infile != outfile: # Copy to <outfile>
        shutil.copyfile(infile, outfile)

    # Open destination file
    with netCDF4.Dataset(outfile, mode='a') as nc:
        # Add epoch global attribute
        nc.epoch = datetime.strptime(epoch, informat).strftime(outformat)

if __name__ == "__main__":
    ncepoch()
