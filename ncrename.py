#!/usr/bin/env python3
# -*- coding: utf-8 -*-
__author__ = "Brian Højen-Sørensen"
__contact__ = "brs@fcoo.dk"
__copyright__ = "Copyright 2020, Danish Defence"
__license__ = "MIT License"
__version__ = "1.0"
"""
Purpose: Rename variables and dimensions in NetCDF files.

Usage: Called from command line. run python3 ncrename.py -h for further
    information.
"""

# Standard library imports
import sys
import shutil
import os
import argparse

# External library imports
import netCDF4

def ncrename():
    # Get my own name
    scriptname = os.path.basename(__file__)

    # Get command line arguments
    parser = argparse.ArgumentParser(
        description ="""Rename variables and dimensions in NetCDF files""")

    parser.add_argument(dest="infile", type=str, action="store", default="",
                        help="path to source file.")

    parser.add_argument(dest="outfile", type=str, action="store", default="",
                        help="filename of the output file.")

    parser.add_argument("-v", "--variable", nargs='?', dest="variables",
                        metavar="...", action="append", type=str, default=[],
                        help="old_var,new_var Variable's old and new names")

    parser.add_argument("-d", "--dmn", "--dimension", nargs='?', dest="dimensions",
                        metavar="...", action="append", type=str, default=[],
                        help="old_dim,new_dim Dimension's old and new names")

    parser.add_argument("-O", "--overwrite", dest="overwrite",
                        action="store_true", default=False,
                        help="Overwrite output file if it exist (or append to \
                              input file).")

    args       = parser.parse_args()
    infile     = args.infile
    variables  = args.variables
    dimensions = args.dimensions
    outfile    = args.outfile
    overwrite  = args.overwrite

    # Does the source file actually exist
    if not os.path.isfile(infile):
        sys.exit(scriptname+': input file: '+infile+': no such file.')

    # Check if we are trying to overwrite any files (unless overwrite = True)
    if not overwrite:
        if os.path.isfile(outfile):
            sys.exit(scriptname+': '+outfile+': file exists. Choose different name or use -O / --overwrite')

    if infile != outfile: # Copy to <outfile>
        shutil.copyfile(infile, outfile)

    # Open destination file
    with netCDF4.Dataset(outfile, mode='a') as nc:
        # Rename dimensions
        for dim in dimensions:
            dim_old, dim_new = dim.split(',')
            if dim_old in nc.dimensions:
                nc.renameDimension(dim_old, dim_new)
            else:
                sys.exit('{}: Dimension not found: {}'.format(scriptname, dim_old))

        # Rename variables
        for var in variables:
            var_old, var_new = var.split(',')
            if var_old in nc.variables:
                nc.renameVariable(var_old, var_new)
            else:
                sys.exit('{}: Variable not found: {}'.format(scriptname, var_old))

if __name__ == "__main__":
    ncrename()
